def changeData():
    import pickle
    with open('data/pickle/items.pickle','rb') as handle:
        items = pickle.load(handle)
    buffer = {}
    for item in items:
        if buffer.get(items[item][1]) == None:
            buffer.update({ items[item][1] : [] })
        buffer[items[item][1]].append(item)
    with open('data/pickle/discipline.pickle','wb') as handle:
        pickle.dump(buffer,handle)