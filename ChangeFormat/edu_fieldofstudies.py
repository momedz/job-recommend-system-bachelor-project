def changeJson():
    import json
    handle = open('data/edu_field.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if len(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "EduField Data : " + str(len(buffer))
    with open('data/json/edu_field.json','w') as handle:
        json.dump(buffer,handle)

def changePickle():
    import pickle
    handle = open('data/edu_field.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if len(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "EduField Data : " + str(len(buffer))
    with open('data/pickle/edu_field.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeData():
    import json
    import pickle
    with open('data/json/edu_field.json','r') as handle:
        target = json.load(handle)
    buffer = {}
    for user in target:
        buffer.update({ int(user) : [] })
        for key in target[user]:
            buffer[int(user)].append(int(key))
    with open('data/json/edu_field.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/edu_field.pickle','wb') as handle:
        pickle.dump(buffer,handle)