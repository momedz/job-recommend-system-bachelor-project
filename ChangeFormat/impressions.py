def changeJson():
    import json
    handle = open('data/impressions.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split('\t')
        id = int(line[0])
        line.remove(line[0])
        year = int(line[0])
        line.remove(line[0])
        week = int(line[0])
        line.remove(line[0])
        line = line[0].split(',')
        if int(line[0]):
            if buffer.get(id) == None:
                buffer.update( { id : {} } );
            if buffer[id].get(year) == None:
                buffer[id].update( { year : {} } )
            if buffer[id][year].get(week) == None:
                buffer[id][year].update( { week : map(int ,line) } )
    print "Impressions Data : " + str(len(buffer))
    with open('data/json/impressions.json','w') as handle:
        json.dump(buffer,handle)

def changePickle():
    import pickle
    handle = open('data/impressions.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split('\t')
        id = int(line[0])
        line.remove(line[0])
        year = int(line[0])
        line.remove(line[0])
        week = int(line[0])
        line.remove(line[0])
        line = line[0].split(',')
        if int(line[0]):
            if buffer.get(id) == None:
                buffer.update( { id : {} } );
            if buffer[id].get(year) == None:
                buffer[id].update( { year : {} } )
            if buffer[id][year].get(week) == None:
                buffer[id][year].update( { week : map(int ,line) } )
    print "Impressions Data : " + str(len(buffer))
    with open('data/pickle/impressions.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeData():
    import json
    import pickle
    with open('data/json/impressions.json','r') as handle:
        target = json.load(handle)
    buffer = {}
    for user in target:
        buffer.update({ int(user) : [] })
        for key in target[user]:
            buffer[int(user)].append(int(key))
    with open('data/json/impressions.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/impressions.pickle','wb') as handle:
        pickle.dump(buffer,handle)