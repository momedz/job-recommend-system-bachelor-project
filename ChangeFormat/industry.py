def changeData():
    import pickle
    with open('data/pickle/items.pickle','rb') as handle:
        items = pickle.load(handle)
    buffer = {}
    for item in items:
        if buffer.get(items[item][2]) == None:
            buffer.update({ items[item][2] : [] })
        buffer[items[item][2]].append(item)
    with open('data/pickle/industry.pickle','wb') as handle:
        pickle.dump(buffer,handle)