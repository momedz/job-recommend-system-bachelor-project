def changeJson():
    import json
    import pandas
    with open('data/items.csv','r') as handle:
        items = pandas.read_csv(handle)
    item_name_id = []
    # id | career_level | discipline_id | industry_id | country | region | latitude | longitude | employment | created_at | active_during_test
    for name_id in items:
        item_name_id.append(name_id)
    print item_name_id
    buffer = {}
    buffer.update({ 'key' : item_name_id })
    for i in range(len(items[item_name_id[0]])):
        print i
        for name_id in item_name_id:
            if name_id == item_name_id[0]:
                buffer.update({ items[name_id][i] : [] })
            else:
                buffer[items[item_name_id[0]][i]].append(items[name_id][i])
    print len(buffer)
    with open('data/json/items.json','w') as handle:
        json.dump(buffer,handle)

def changeData():
    import json
    import math
    import pickle
    with open('data/json/items.json','r') as handle:
        items = json.load(handle)
    key = items.pop('key')
    buffer = { 'key' : key }
    for data in items:
        buffer.update({ int(data) : [] })
        print items[data]
        for i in range(len(items[data])):
            if i == 3:
                buffer[int(data)].append(str(items[data][i]))
            elif i == 5 or i == 6:
                buffer[int(data)].append(float(items[data][i]))
            elif items[data][i] == None or math.isnan(items[data][i]):
                buffer[int(data)].append(None)
            else:
                buffer[int(data)].append(int(items[data][i]))
    print len(buffer)
    with open('data/json/items.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/items.pickle','wb') as handle:
        pickle.dump(buffer,handle)