def changeJson():
    import json
    handle = open('data/jobroles.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if int(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "Jobroles Data : " + str(len(buffer))
    with open('data/json/jobroles.json','w') as handle:
        json.dump(buffer,handle)

def changePickle():
    import pickle
    handle = open('data/jobroles.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if int(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "Jobroles Data : " + str(len(buffer))
    with open('data/pickle/jobroles.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeData():
    import json
    import pickle
    with open('data/json/jobroles.json','r') as handle:
        target = json.load(handle)
    buffer = {}
    for user in target:
        buffer.update({ int(user) : [] })
        for key in target[user]:
            buffer[int(user)].append(int(key))
    with open('data/json/jobroles.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/jobroles.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeKey():
    import pickle
    import json
    with open('data/pickle/jobroles.pickle','rb') as handle:
        jobroles = pickle.load(handle);

    buffer = {}
    for object in jobroles:
        for key in jobroles[object]:
            if buffer.get(key) == None:
                buffer.update({ key : [] })
            buffer[key].append(object)
    print "Key jobrole Data : " + str(len(buffer))
    with open('data/pickle/keyjobroles.pickle','wb') as handle:
        pickle.dump(buffer,handle)
    with open('data/json/keyjobroles.json','w') as handle:
        json.dump(buffer,handle)