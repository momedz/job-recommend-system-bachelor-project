def changeJson():
    import json
    handle = open('data/tags.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if len(line[0]):
            if buffer.get(id) == None:
                buffer.update( { id : map(int ,line) } )
    print "Tags Data : " + str(len(buffer))
    with open('data/json/tags.json','w') as handle:
        json.dump(buffer,handle)

def changePickle():
    import pickle
    handle = open('data/tags.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if len(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "Tags Data : " + str(len(buffer))
    with open('data/pickle/tags.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeData():
    import json
    import pickle
    with open('data/json/tags.json','r') as handle:
        target = json.load(handle)
    buffer = {}
    for item in target:
        buffer.update({ int(item) : [] })
        for key in target[item]:
            buffer[int(item)].append(int(key))
    with open('data/json/tags.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/tags.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeKey():
    import pickle
    import json
    with open('data/pickle/tags.pickle','rb') as handle:
        tags = pickle.load(handle);

    buffer = {}
    for object in tags:
        for key in tags[object]:
            if buffer.get(key) == None:
                buffer.update({ key : [] })
            buffer[key].append(object)
    print "Key tags Data : " + str(len(buffer))
    with open('data/pickle/keytags.pickle','wb') as handle:
        pickle.dump(buffer,handle)
    with open('data/json/keytags.json','w') as handle:
        json.dump(buffer,handle)