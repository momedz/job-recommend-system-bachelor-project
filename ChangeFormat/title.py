def changeJson():
    import json
    handle = open('data/title.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if len(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "Title Data : " + str(len(buffer))
    with open('data/json/title.json','w') as handle:
        json.dump(buffer,handle)

def changePickle():
    import pickle
    handle = open('data/title.csv', 'r')
    handle.readline()

    buffer = {}
    for line in handle:
        line = line.split('\n')[0].split(',')
        id = int(line[0])
        line.remove(line[0])
        if len(line[0]):
            buffer.update( { id : map(int ,line) } )
    print "Title Data : " + str(len(buffer))
    with open('data/pickle/title.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeData():
    import json
    import pickle
    with open('data/json/title.json','r') as handle:
        target = json.load(handle)
    buffer = {}
    for item in target:
        buffer.update({ int(item) : [] })
        for key in target[item]:
            buffer[int(item)].append(int(key))
    with open('data/json/title.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/title.pickle','wb') as handle:
        pickle.dump(buffer,handle)

def changeKey():
    import pickle
    import json
    with open('data/pickle/title.pickle','rb') as handle:
        title = pickle.load(handle);

    buffer = {}
    for object in title:
        for key in title[object]:
            if buffer.get(key) == None:
                buffer.update({ key : [] })
            buffer[key].append(object)
    print "Key title Data : " + str(len(buffer))
    with open('data/pickle/keytitle.pickle','wb') as handle:
        pickle.dump(buffer,handle)
    with open('data/json/keytitle.json','w') as handle:
        json.dump(buffer,handle)