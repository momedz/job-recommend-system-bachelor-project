def changeJson():
    import json
    import pandas
    with open('data/users.csv','r') as handle:
        users = pandas.read_csv(handle)
    user_name_id = []
    # id | career_level | discipline_id | industry_id | country | region | experience_n_entries_class | experience_years_experience | experience_years_in_current | edu_degree
    for name_id in users:
        user_name_id.append(name_id)
    buffer = {}
    buffer.update({ 'key' : user_name_id })
    for i in range(len(users[user_name_id[0]])):
        for name_id in user_name_id:
            if name_id == user_name_id[0]:
                buffer.update({ users[name_id][i] : [] })
            else:
                buffer[users[user_name_id[0]][i]].append(users[name_id][i])
    print len(buffer)
    with open('data/json/users.json','w') as handle:
        json.dump(buffer,handle)

def changeData():
    import json
    import math
    import pickle
    with open('data/json/users.json','r') as handle:
        users = json.load(handle)
    key = users.pop('key')
    buffer = { 'key' : key }
    for data in users:
        buffer.update({ int(data) : [] })
        print users[data]
        for i in range(len(users[data])):
            if i == 3:
                buffer[int(data)].append(str(users[data][i]))
            elif math.isnan(users[data][i]):
                buffer[int(data)].append(None)
            else:
                buffer[int(data)].append(int(users[data][i]))
    with open('data/json/users.json','w') as handle:
        json.dump(buffer,handle)
    with open('data/pickle/users.pickle','wb') as handle:
        pickle.dump(buffer,handle)