def dataset1():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/out/good_1.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    gc.disable()
    time_start = time.time()
    for user in data:
        buffer.update({ user: {} })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].update({item: value})
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/out/good_data_1.pickle')
    del data
    del tags
    del buffer

def dataset2():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/out/good_2.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    gc.disable()
    time_start = time.time()
    for user in data:
        buffer.update({ user: {} })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].update({item: value})
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/out/good_data_2.pickle')
    del data
    del tags
    del buffer

def dataset3():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/out/bad_1.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    gc.disable()
    time_start = time.time()
    for user in data:
        buffer.update({ user: {} })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].update({item: value})
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/out/bad_data_1.pickle')
    del data
    del tags
    del buffer

def dataset4():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/out/bad_2.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    gc.disable()
    time_start = time.time()
    for user in data:
        buffer.update({ user: {} })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].update({item: value})
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/out/bad_data_2.pickle')
    del data
    del tags
    del buffer