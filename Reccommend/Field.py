def field1():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    jobroles = Filepickle.read('data/pickle/jobroles.pickle')
    keytitle = Filepickle.read('data/pickle/keytitle.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 1 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({user: {'item': {}, 'lostkey': []}})
        if user in jobroles:
            for key in jobroles[user]:
                if key in keytitle:
                    for item in keytitle[key]:
                        if items[item][9] == 1:
                            if users[user][0] == None:
                                users[user][0] = 0
                            if users[user][0] + 1 >= items[item][0] and users[user][0] - 2 <= items[item][0] and users[user][0] != 0:
                                if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                                    if items[item][4] == 0 and items[item][3] == 'de':
                                        continue
                                    if users[user][4] == items[item][4]:
                                        if buffer[user]['item'].get(item) == None:
                                            buffer[user]['item'].update({item: 0})
                                        buffer[user]['item'][item] += 1
                else:
                    if key in buffer[user]['lostkey']:
                        continue
                    else:
                        buffer[user]['lostkey'].append(key)
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field1.pickle')

def field2():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    jobroles = Filepickle.read('data/pickle/jobroles.pickle')
    keytitle = Filepickle.read('data/pickle/keytitle.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 2 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({user: {'item': {}, 'lostkey': []}})
        if user in jobroles:
            for key in jobroles[user]:
                if key in keytitle:
                    for item in keytitle[key]:
                        if items[item][9] == 1:
                            if users[user][0] == None:
                                users[user][0] = 0
                            if users[user][0] == 0:
                                if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                                    if items[item][4] == 0 and items[item][3] == 'de':
                                        continue
                                    if users[user][4] == items[item][4]:
                                        if buffer[user]['item'].get(item) == None:
                                            buffer[user]['item'].update({item: 0})
                                        buffer[user]['item'][item] += 1
                else:
                    if key in buffer[user]['lostkey']:
                        continue
                    else:
                        buffer[user]['lostkey'].append(key)
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field2.pickle')

def field3():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    jobroles = Filepickle.read('data/pickle/jobroles.pickle')
    keytitle = Filepickle.read('data/pickle/keytitle.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 3 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({user: {'item': {}, 'lostkey': []}})
        if user in jobroles:
            for key in jobroles[user]:
                if key in keytitle:
                    for item in keytitle[key]:
                        if items[item][9] == 0:
                            if users[user][0] == None:
                                users[user][0] = 0
                            if users[user][0] + 1 >= items[item][0] and users[user][0] - 2 <= items[item][0] and users[user][0] != 0:
                                if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                                    if items[item][4] == 0 and items[item][3] == 'de':
                                        continue
                                    if users[user][4] == items[item][4]:
                                        if buffer[user]['item'].get(item) == None:
                                            buffer[user]['item'].update({item: 0})
                                        buffer[user]['item'][item] += 1
                else:
                    if key in buffer[user]['lostkey']:
                        continue
                    else:
                        buffer[user]['lostkey'].append(key)
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field3.pickle')

def field4():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    jobroles = Filepickle.read('data/pickle/jobroles.pickle')
    keytitle = Filepickle.read('data/pickle/keytitle.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 4 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({user: {'item': {}, 'lostkey': []}})
        if user in jobroles:
            for key in jobroles[user]:
                if key in keytitle:
                    for item in keytitle[key]:
                        if items[item][9] == 0:
                            if users[user][0] == None:
                                users[user][0] = 0
                            if users[user][0] == 0:
                                if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                                    if items[item][4] == 0 and items[item][3] == 'de':
                                        continue
                                    if users[user][4] == items[item][4]:
                                        if buffer[user]['item'].get(item) == None:
                                            buffer[user]['item'].update({item: 0})
                                        buffer[user]['item'][item] += 1
                else:
                    if key in buffer[user]['lostkey']:
                        continue
                    else:
                        buffer[user]['lostkey'].append(key)
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field4.pickle')

def field5():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 5 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] == 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 1:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] + 1 >= items[item][0] and users[user][0] - 2 <= items[item][0] and users[user][0] != 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 2 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field5.pickle')

def field6():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 6 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] == 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 1:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] == 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 2 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field6.pickle')

def field7():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 7 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] == 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 0:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] + 1 >= items[item][0] and users[user][0] - 2 <= items[item][0] and users[user][0] != 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 2 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field7.pickle')

def field8():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 8 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] == 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 0:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] == 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 2 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field8.pickle')

def field9():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 9 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] != 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 1:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] + 1 >= items[item][0] and users[user][0] - 2 <= items[item][0] and users[user][0] != 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 1 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field9.pickle')

def field10():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 10 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] != 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 1:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] == 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 1 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field10.pickle')

def field11():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 11 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] != 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 0:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] + 1 >= items[item][0] and users[user][0] - 2 <= items[item][0] and users[user][0] != 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 1 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field11.pickle')

def field12():
    import Filepickle
    import time
    import sys
    import gc
    users = Filepickle.read('data/pickle/users.pickle')
    items = Filepickle.read('data/pickle/items.pickle')
    disindus = Filepickle.read('data/pickle/disindus.pickle')
    buffer = {}
    i = 0
    users_name_id = users.pop('key')
    items_name_id = items.pop('key')
    gc.disable()
    time_start = time.time()
    for user in users:
        if i % int(len(users) / 10000) == 0:
            print "Field 12 : " + str(float(int((float(i) / float(len(users))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: {} })
        if users[user][1] != 0 or users[user][2] == 0:
            continue
        for item in disindus[users[user][1]][users[user][2]]:
            if items[item][9] == 0:
                if users[user][0] == None:
                    users[user][0] = 0
                if users[user][0] == 0:
                    if users[user][3] == items[item][3] and items[item][3] != 'non_dach':
                        if items[item][4] == 0 and items[item][3] == 'de':
                            continue
                        if users[user][4] == items[item][4]:
                            if buffer[user].get(item) == None:
                                buffer[user].update( { item : 1 } )
                            else:
                                buffer[user][item] += 1
    gc.enable()
    Filepickle.write(buffer,'data/pickle/field12.pickle')