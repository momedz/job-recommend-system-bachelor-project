def read(file,gcmode=True):
    import time
    import cPickle as pickle
    import gc

    start = time.time()
    print "File : Start read File \'" + str(file) + "\'"
    if gcmode:
        gc.disable()
    with open(file,'rb') as handle:
        data = pickle.load(handle)
    gc.enable()
    print "File : End read File \'" + str(file) + "\' | Time : " + str(time.time() - start)
    return data

def proxytodict(data):
    return data['dict_proxy']

def write(data,file):
    import time
    import cPickle as pickle
    start = time.time()
    print "File : Start write File \'" + str(file) + "\'"
    with open(file,'wb') as handle:
        pickle.dump(data,handle)
    print "File : End write File \'" + str(file) + "\' | Time : " + str(time.time() - start)