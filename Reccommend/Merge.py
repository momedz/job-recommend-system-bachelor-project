from Reccommend import Filepickle


def result1():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    good1 = Filepickle.read('data/pickle/merge/very_good_data_1.pickle')
    good2 = Filepickle.read('data/pickle/merge/very_good_data_2.pickle')
    gc.disable()
    time_start = time.time()
    buffer = {}
    i = 0
    for user in good1:
        if i % int(len(good1) / 10000) == 0:
            print "Result 1 : " + str(float(int((float(i) / float(len(good1))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        if user in good2:
            for item in good1[user]:
                if item in good2[user]:
                    if buffer.get(user) == None:
                        buffer.update( { user : [] } )
                    buffer[user].append(item)
    gc.enable()
    del good1
    del good2
    Filepickle.write(buffer,'data/pickle/merge/result1.pickle')

def result2():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    good1 = Filepickle.read('data/pickle/merge/very_good_data_1.pickle')
    result1 = Filepickle.read('data/pickle/merge/result1.pickle')
    gc.disable()
    time_start = time.time()
    buffer = {}
    i = 0
    for user in good1:
        if i % int(len(good1) / 10000) == 0:
            print "Result 2 : " + str(float(int((float(i) / float(len(good1))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        if user in result1:
            for item in good1[user]:
                if item in result1[user]:
                    continue
                else:
                    if buffer.get(user) == None:
                        buffer.update( { user : [] } )
                    buffer[user].append(item)
        else:
            buffer[user] = good1[user]
    gc.enable()
    del good1
    del result1
    Filepickle.write(buffer,'data/pickle/merge/result2.pickle')

def result3():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    good2 = Filepickle.read('data/pickle/merge/very_good_data_2.pickle')
    result1 = Filepickle.read('data/pickle/merge/result1.pickle')
    gc.disable()
    time_start = time.time()
    buffer = {}
    i = 0
    for user in good2:
        if i % int(len(good2) / 10000) == 0:
            print "Result 3 : " + str(float(int((float(i) / float(len(good2))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        if user in result1:
            for item in good2[user]:
                if item in result1[user]:
                    continue
                else:
                    if buffer.get(user) == None:
                        buffer.update( { user : [] } )
                    buffer[user].append(item)
        else:
            buffer[user] = good2[user]
    gc.enable()
    del good2
    del result1
    Filepickle.write(buffer,'data/pickle/merge/result3.pickle')

def result4():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    bad1 = Filepickle.read('data/pickle/merge/very_bad_data_1.pickle')
    bad2 = Filepickle.read('data/pickle/merge/very_bad_data_2.pickle')
    gc.disable()
    time_start = time.time()
    buffer = {}
    i = 0
    for user in bad1:
        if i % int(len(bad1) / 10000) == 0:
            print "Result 4 : " + str(float(int((float(i) / float(len(bad1))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        if user in bad2:
            for item in bad1[user]:
                if item in bad2[user]:
                    if buffer.get(user) == None:
                        buffer.update( { user : [] } )
                    buffer[user].append(item)
    gc.enable()
    del bad1
    del bad2
    Filepickle.write(buffer,'data/pickle/merge/result4.pickle')

def result5():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    bad1 = Filepickle.read('data/pickle/merge/very_bad_data_1.pickle')
    result4 = Filepickle.read('data/pickle/merge/result4.pickle')
    gc.disable()
    time_start = time.time()
    buffer = {}
    i = 0
    for user in bad1:
        if i % int(len(bad1) / 10000) == 0:
            print "Result 5 : " + str(float(int((float(i) / float(len(bad1))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        if user in result4:
            for item in bad1[user]:
                if item in result4[user]:
                    continue
                else:
                    if buffer.get(user) == None:
                        buffer.update( { user : [] } )
                    buffer[user].append(item)
        else:
            buffer[user] = bad1[user]
    gc.enable()
    del bad1
    del result4
    Filepickle.write(buffer,'data/pickle/merge/result5.pickle')

def result6():
    import Reccommend.Filepickle as Filepickle
    import time
    import sys
    import gc
    bad2 = Filepickle.read('data/pickle/merge/very_bad_data_2.pickle')
    result4 = Filepickle.read('data/pickle/merge/result4.pickle')
    gc.disable()
    time_start = time.time()
    buffer = {}
    i = 0
    for user in bad2:
        if i % int(len(bad2) / 10000) == 0:
            print "Result 6 : " + str(float(int((float(i) / float(len(bad2))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start))  + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        if user in result4:
            for item in bad2[user]:
                if item in result4[user]:
                    continue
                else:
                    if buffer.get(user) == None:
                        buffer.update( { user : [] } )
                    buffer[user].append(item)
        else:
            buffer[user] = bad2[user]
    gc.enable()
    del bad2
    del result4
    Filepickle.write(buffer,'data/pickle/merge/result6.pickle')