def dataset1():
    import Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/merge/good_data_1.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    i = 0
    gc.disable()
    time_start = time.time()
    for user in data:
        if i % int(len(data) / 10000) == 0:
            print "Field Tagset 1 : " + str(float(int((float(i) / float(len(data))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: [] })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].append(item)
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/merge/very_good_data_1.pickle')
    del data
    del tags
    del buffer

def dataset2():
    import Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/merge/good_data_2.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    i = 0
    gc.disable()
    time_start = time.time()
    for user in data:
        if i % int(len(data) / 10000) == 0:
            print "Field Tagset 2 : " + str(float(int((float(i) / float(len(data))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: [] })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].append(item)
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/merge/very_good_data_2.pickle')
    del data
    del tags
    del buffer

def dataset3():
    import Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/merge/bad_data_1.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    i = 0
    gc.disable()
    time_start = time.time()
    for user in data:
        if i % int(len(data) / 10000) == 0:
            print "Field Tagset 3 : " + str(float(int((float(i) / float(len(data))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: [] })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].append(item)
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/merge/very_bad_data_1.pickle')
    del data
    del tags
    del buffer

def dataset4():
    import Filepickle
    import time
    import sys
    import gc
    import operator
    data = Filepickle.read('data/pickle/merge/bad_data_2.pickle')
    tags = Filepickle.read('data/pickle/tags.pickle')
    buffer = {}
    i = 0
    gc.disable()
    time_start = time.time()
    for user in data:
        if i % int(len(data) / 10000) == 0:
            print "Field Tagset 4 : " + str(float(int((float(i) / float(len(data))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec | size data : " + str(sys.getsizeof(buffer)) + " bytes"
        i += 1
        buffer.update({ user: [] })
        if len(data[user]) == 0:
            continue
        buffer_tag = {}
        for item in data[user]:
            if item in tags:
                for tag in tags[item]:
                    if buffer_tag.get(tag) == None:
                        buffer_tag.update( { tag : 0 } )
                    buffer_tag[tag] += data[user][item]
        buffer_item = {}
        for item in data[user]:
            buffer_item.update( { item : data[user][item] } )
            if item in tags:
                for tag in tags[item]:
                    buffer_item[item] += buffer_tag[tag] * 0.0001
        buffer_item = sorted(buffer_item.items(), key=operator.itemgetter(1))
        for item, value in buffer_item:
            buffer[user].append(item)
        del buffer_tag
        del buffer_item
    gc.enable()
    Filepickle.write(buffer,'data/pickle/merge/very_bad_data_2.pickle')
    del data
    del tags
    del buffer