def filter(file,amount=1):
    import Reccommend.Filepickle as Filepickle
    import time
    input_data = Filepickle.read('data/pickle/'+file+'.pickle')
    data = {}
    i = 1
    time_start = time.time()
    for user in input_data:
        data.update({ user : {} })
        if i % int(len(input_data) / 10000) == 0:
            print file + " : " + str(float(int((float(i) / float(len(input_data))) * 10000)) / float(100) + 0.01) + " % | time : " + str(int(time.time()-time_start)) + " sec"
        i += 1
        if len(input_data[user]['item']) == 0:
            continue
        while len(data[user]) < amount and ( len(data[user]) != len(input_data[user]['item']) ):
            buffer = input_data[user]['item']
            max = 0
            top = {}
            for item in data[user]:
                if item in buffer:
                    buffer.pop(item)
            for item in buffer:
                if buffer[item] > max:
                    max = buffer[item]
                    top = {}
                if buffer[item] == max:
                    top.update({ item : max })
            data[user] = dict( data[user].items() + top.items() )
    Filepickle.write(data,'data/pickle/top/'+file+'.pickle')
    del input_data
    del data