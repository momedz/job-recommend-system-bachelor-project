import Field
import Amusementparks
import Topfilter
import Tagset
import Merge
def choose():
    target = input("Input Number for run code ( 0 - 4 ) [ -1 = exit ] : ")
    if target == 0:
        Amusementparks.play()
    if target == 1:
        select_field()
    if target == 2:
        select_topfilter()
    if target == 3:
        select_tagset()
    if target == 4:
        select_merge()

def select_field():
    target = input("Input Number for run Field ( 1 - 12 ) [ -1 = exit ] : ")
    if target == 1:
        Field.field1()
    if target == 2:
        Field.field2()
    if target == 3:
        Field.field3()
    if target == 4:
        Field.field4()
    if target == 5:
        Field.field5()
    if target == 6:
        Field.field6()
    if target == 7:
        Field.field7()
    if target == 8:
        Field.field8()
    if target == 9:
        Field.field9()
    if target == 10:
        Field.field10()
    if target == 11:
        Field.field11()
    if target == 12:
        Field.field12()

def select_topfilter():
    Topfilter.filter('field3')
    Topfilter.filter('field4')

def select_tagset():
    target = input("Input Number for run Topset ( 1 - 4 ) [ -1 = exit ] : ")
    if target == 1:
        Tagset.dataset1()
    if target == 2:
        Tagset.dataset2()
    if target == 3:
        Tagset.dataset3()
    if target == 4:
        Tagset.dataset4()

def select_merge():
    target = input("Input Number for run merge ( 1 - 6 ) [ -1 = exit ] : ")
    if target == 1:
        Merge.result1()
    if target == 2:
        Merge.result2()
    if target == 3:
        Merge.result3()
    if target == 4:
        Merge.result4()
    if target == 5:
        Merge.result5()
    if target == 6:
        Merge.result6()